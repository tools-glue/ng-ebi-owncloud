import {
    NgModule
} from '@angular/core';
import {
    HttpClientModule
} from '@angular/common/http';

import {
    JwtModule
} from '@auth0/angular-jwt';
import {
    AuthModule
} from 'ng-ebi-authorization';
import {
    OwncloudModule
} from 'src/app/modules/owncloud/owncloud.module';

@NgModule({
    imports: [
        JwtModule.forRoot({
            config: {
                tokenGetter: () => localStorage.getItem('id_token')
            }
        }),
        AuthModule.forRoot(),
        OwncloudModule.forRoot({
            owncloudURL: 'https://oc.ebi.ac.uk'
        }),
        HttpClientModule,
    ],
    exports: [
        OwncloudModule
    ]
})
export class CommonStub {}
