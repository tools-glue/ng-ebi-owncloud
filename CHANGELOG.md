<a name="0.1.0-beta.4"></a>
# [0.1.0-beta.4](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/compare/0.1.0-beta.3...0.1.0-beta.4) (2019)

### Features
* avoid making http requests to oC if user is not login ([e8b2836](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/commit/e8b2836), closes [#7](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/issues/7)
* nicer message if file or directory doesn't exit ([08c5b80](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/commit/08c5b80), closes [#8](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/issues/8)
* nicer message if directory doesn't contain any files ([ccb73bc](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/commit/ccb73bc)

<a name="0.1.0-beta.3"></a>
# [0.1.0-beta.3](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/compare/0.1.0-beta.2...0.1.0-beta.3) (2018-10-23)

### Features
* upgrade to angular version 7

<a name="0.1.0-beta.2"></a>
# [0.1.0-beta.2](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/compare/0.1.0-beta.1...0.1.0-beta.2) (2018-09-10)

### Features
* corrected documentation in README file

<a name="0.1.0-beta.1"></a>
# [0.1.0-beta.1](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/compare/0.1.0-alpha.11...0.1.0-beta.1) (2018-07-12)

### Features
* updated to the latest version of ng-ebi-authorization library

<a name="0.1.0-alpha.11"></a>
# [0.1.0-alpha.11](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/compare/0.1.0-alpha.10...0.1.0-alpha.11) (2018-06-29)

### Features
* avoid unnecessary calls to ownCloud if the user is not authenticated

<a name="0.1.0-alpha.10"></a>
# [0.1.0-alpha.10](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/compare/0.1.0-alpha.9...0.1.0-alpha.10) (2018-06-26)

### Features
* use classes for content projection instead of elements

<a name="0.1.0-alpha.9"></a>
# [0.1.0-alpha.9](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/compare/0.1.0-alpha.8...0.1.0-alpha.9) (2018-06-26)

### Features
* use content projection to display waiting message.

<a name="0.1.0-alpha.8"></a>
# [0.1.0-alpha.8](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/compare/0.1.0-alpha.7...0.1.0-alpha.8) (2018-06-25)

### Features
* provides `forChild` method to register component without provider

<a name="0.1.0-alpha.7"></a>
# [0.1.0-alpha.7](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/compare/0.1.0-alpha.6...0.1.0-alpha.7) (2018-06-20)

### Features
* removes HttpClientModule as module import/export
* updated libraries
* simplify testing

<a name="0.1.0-alpha.6"></a>
# [0.1.0-alpha.6](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/compare/0.1.0-alpha.5...0.1.0-alpha.6) (2018-06-18)

### Features
* upgraded angular-app-auth: JwtModule must be configured independently

<a name="0.1.0-alpha.5"></a>
# [0.1.0-alpha.5](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/compare/0.1.0-alpha.4...0.1.0-alpha.5) (2018-05-16)

### Features
* upgraded angular-app-auth version and drop rxjs-compat
* substituted `moment` by `date-fns`

<a name="0.1.0-alpha.4"></a>
# [0.1.0-alpha.4](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/compare/0.1.0-alpha.3...0.1.0-alpha.4) (2018-05-08)

### Features
* updated to Angular version 6

<a name="0.1.0-alpha.3"></a>
# [0.1.0-alpha.3](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/compare/0.1.0-alpha.2...0.1.0-alpha.3) (2018-03-27)

### Features
* **OwncloudService** added new utility method `isDirectory` ([ce7d6fc](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/commit/ce7d6fc))
* **FilepickerComponent** added new property to show/hide back arrow ([ce7d6fc](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/commit/ce7d6fc))

<a name="0.1.0-alpha.2"></a>
# [0.1.0-alpha.2](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/compare/0.1.0-alpha.1...0.1.0-alpha.2) (2018-02-22)

### Features

* **OwncloudService:** removed untested code ([04bc2bc6](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/commit/04bc2bc6)), closes [#5](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/issues/5
* **OwncloudService:** included HttpClientModule for easy use ([04bc2bc6](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/commit/04bc2bc6)), closes [#3](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/issues/3)
* **OwncloudModule** more specific export.

<a name="0.1.0-alpha.1"></a>
# [0.1.0-alpha.1](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/compare/0.1.0-alpha.0...0.1.0-alpha.1) (2018-02-20)

### Features

* **OwncloudService** exported and available independently from the component ([7bafc6d6](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/commit/7bafc6d6)), closes [#2](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/issues/2)

<a name="0.1.0-alpha.0"></a>
# [0.1.0-alpha.0](https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/tree/0.1.0-alpha.0) (2018-02-20)

### Features

* Polling refresh and OwncloudService that works exclusively via webDAV.
