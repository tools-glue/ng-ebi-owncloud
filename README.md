# ng-ebi-owncloud

The ng-ebi-owncloud is an Angular library that provides a component and a
services to interact with EBI's ownCloud. The library relies on EBI's
Authentication and Authorization Profile (AAP) infrastructure. After successful
login, a JWT token is stored on the browser (via cookie, local or session
storage). The token is used to request resources from ownCloud, such as listing
directories, reading files or creating files and directories, among others.

## Installation

To install this library, run:

```
npm install --save @angular/cdk @angular/material
npm install --save ng-ebi-owncloud ng-ebi-authorization @auth0/angular-jwt date-fns@next

or

yarn add @angular/cdk @angular/material
yarn add ng-ebi-owncloud ng-ebi-authorization @auth0/angular-jwt date-fns@next
```

| Angular version | ng-ebi-owncloud version |
| --------------- | ----------------------- |
| >=5 <6          | <=0.1.0-alpha.3         |
| >=6 <8          | >=0.1.0-alpha.4         |

## Consuming the library

Add [theming](https://material.angular.io/guide/theming) to the application. For
example, add to the following import to your `src/style.css`:

```
@import '~@angular/material/prebuilt-themes/deeppurple-amber.css';
```

Add Google Material Icons to your `index.html`:

```
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
```

In your Angular `AppModule` (app.module.ts):

```ts
import {
    BrowserModule
} from '@angular/platform-browser';
import {
    NgModule
} from '@angular/core';
import {
    HttpClientModule
} from '@angular/common/http';

import {
    AppComponent
} from './app.component';
import {
    JwtModule
} from '@auth0/angular-jwt';
import {
    AuthModule
} from 'ng-ebi-authorization';
import {
    OwncloudModule
} from 'ng-ebi-owncloud';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        JwtModule.forRoot({
            config: {
                tokenGetter: () => localStorage.getItem('id_token')
            }
        }),
        AuthModule.forRoot(),
        OwncloudModule.forRoot({
            owncloudURL: 'https://oc.ebi.ac.uk'
        }),
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
```

Basic example of using the component:

```ts
import {
    Component,
    OnInit
} from '@angular/core';

import {
    AuthService,
} from 'ng-ebi-authorization';

@Component({
    selector: 'app-root',
    template: `
    <button (click)="auth.openLoginTab()">Login</button>
    <button (click)="auth.logOut()">Logout</button>

    <oc-filepicker></oc-filepicker>
    `
})
export class AppComponent implements OnInit {

    constructor(
        // Public for demonstration purposes
        public auth: AuthService,
    ) {}

    ngOnInit() {}
}
```

Advance example of using the component:

```ts
import {
    Component,
    OnInit
} from '@angular/core';

import {
    AuthService,
} from 'ng-ebi-authorization';

@Component({
    selector: 'app-root',
    template: `
    <button (click)="auth.openLoginTab()">Login</button>
    <button (click)="auth.logOut()">Logout</button>

    <oc-filepicker [directory]="directory" [showBackArrow]="showBackArrow" [showHidden]="showHidden" [download]="download" [navigate]="navigate" [refreshInterval]="pollingInterval" (path)="onPath($event)"></oc-filepicker>
    `
})
export class AppComponent implements OnInit {
    directory = '';
    showBackArrow = false;
    showHidden = true;
    download = true;
    navigate = true;
    pollingInterval = 10;

    constructor(
        // Public for demonstration purposes
        public auth: AuthService,
    ) {}

    ngOnInit() {}

    onPath(path: string): void {
        console.log('New path selected: ', path);
    }
}
```

Example use of the OwncloudService:

```ts
import {
    Component,
    OnInit
} from '@angular/core';
import {
    first
} from 'rxjs/operators';

import {
    AuthService,
} from 'ng-ebi-authorization';
import {
    OwncloudService,
} from 'ng-ebi-owncloud';

@Component({
    selector: 'app-root',
    template: `
    <button (click)="auth.windowOpen()">Login</button>
    <button (click)="auth.logOut()">Logout</button>
    <button (click)="downloadFile()">Download picture</button>
    `
})
export class AppComponent implements OnInit {

    constructor(
        // Public for demonstration purposes
        public auth: AuthService,
        private _oc: OwncloudService
    ) {}

    ngOnInit() {}

    downloadFile() {
        this._oc.downloadFile('/Photos/Squirrel.jpg').pipe(
            first(),
        ).subscribe();
    }
}
```

## Want to help?

Want to file a bug, contribute some code, or improve documentation? Excellent!
Read up on our guidelines for [contributing][contributing].

## License

Apache 2.0 © [EMBL - European Bioinformatics Institute](https://www.ebi.ac.uk/about/terms-of-use)

[contributing]: https://gitlab.ebi.ac.uk/tools-glue/ng-ebi-owncloud/blob/master/CONTRIBUTING.md
