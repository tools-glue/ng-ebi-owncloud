import {
    TestBed,
    async
} from '@angular/core/testing';
import {
    BrowserModule
} from '@angular/platform-browser';
import {
    discardPeriodicTasks,
    fakeAsync,
    flushMicrotasks,
    tick
} from '@angular/core/testing';
import {
    RouterTestingModule
} from '@angular/router/testing';

import {
    CommonStub
} from 'testing/common';

import {
    AppComponent
} from './app.component';
import {
    HttpBackendClient
} from './modules/owncloud/services/httpbackendclient.service';

describe('AppComponent', () => {
    beforeEach(async (() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent
            ],
            imports: [
                BrowserModule,
                RouterTestingModule,
                CommonStub,
            ],
            providers: [
                HttpBackendClient
            ],
        }).compileComponents();
    }));

    it('should create the app', async (() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));

    it('should render title in a h1 tag', fakeAsync(() => {
        const fixture = TestBed.createComponent(AppComponent);
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h1').textContent).toContain('ng-ebi-owncloud testing app');
        discardPeriodicTasks();
    }));
});
