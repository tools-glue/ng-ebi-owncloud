import {
    BrowserModule
} from '@angular/platform-browser';
import {
    NgModule
} from '@angular/core';
import {
    RouterModule,
    Routes
} from '@angular/router';
import {
    HttpClientModule
} from '@angular/common/http';

// Modules
import {
    environment,
} from 'src/environments/environment';
import {
    AuthModule
} from 'ng-ebi-authorization';
import {
    JwtModule
} from '@auth0/angular-jwt';
import {
    OwncloudModule
} from './modules/owncloud/owncloud.module';

// Services
import {
    HttpBackendClient
} from './modules/owncloud/services/httpbackendclient.service';

// Components
import {
    AppComponent
} from './app.component';

export function getToken(): string {
    return localStorage.getItem('jwt_token') || '';
}
export function updateToken(newToken: string): void {
    return localStorage.setItem('jwt_token', newToken);
}
// Optional
export function removeToken(): void {
    return localStorage.removeItem('jwt_token');
}

const routes: Routes = [{
    path: '**',
    component: AppComponent
}];

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes,
          //  { enableTracing: true } // Debugging purposes only
        ),
        HttpClientModule,
        JwtModule.forRoot({
            config: {
                tokenGetter: getToken,
                whitelistedDomains: ['dev.flow.toolchain.ebi.ac.uk'],
                skipWhenExpired: true
            }
        }),
        // AuthModule.forRoot(),
        AuthModule.forRoot({
            aapURL: 'https://api.aai.ebi.ac.uk',
            tokenGetter: getToken,
            tokenUpdater: updateToken,
            tokenRemover: removeToken // Optional
        }),
        OwncloudModule.forRoot({
            owncloudURL: environment.owncloudURL
        }),
    ],
    providers: [
        /**
         * This custom version of HttpClient ignores interceptors.
         * Use this HttpClient to avoid sending JWT tokens in the header 'Authorization: Basic`
         * in every http request.
         */
        HttpBackendClient

    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
