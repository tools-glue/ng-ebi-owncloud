import {
    InjectionToken
} from '@angular/core';

export interface OwncloudConfig {
    owncloudURL: string;
}

export const OC_CONFIG = new InjectionToken < OwncloudConfig > ('OC_CONFIG');
