import {
    Component,
    OnInit,
    OnChanges,
    SimpleChanges,
    Input,
    Output,
    EventEmitter
} from '@angular/core';
import {
    HttpErrorResponse
} from '@angular/common/http';
import {
    Observable,
    of ,
    timer
} from 'rxjs';
import {
    catchError,
    filter,
    first,
    map,
    mergeMap,
    tap,
} from 'rxjs/operators';
// import {
//     trigger,
//     state,
//     style,
//     animate,
//     transition
// } from '@angular/animations';

import {
    AuthService
} from 'ng-ebi-authorization';
import {
    OwncloudService,
    DirectoryListing,
    FileListing,
    dirname
} from '../../services/owncloud.service';
import {
    HumanreadableService
} from '../../services/humanreadable.service';

export interface RicherListing extends DirectoryListing {
    files: RicherFileListing[];
}

export interface RicherFileListing extends FileListing {
    lastModified: string;
    humanSize: string;
    icon: string;
}

@Component({
    selector: 'oc-filepicker',
    templateUrl: './filepicker.component.html',
    styleUrls: ['./filepicker.component.css'],
    // No used
    // animations: [
    //     trigger('fade', [
    //         state('in', style({
    //             opacity: 1
    //         })),
    //         state('void', style({
    //             opacity: 0
    //         })),
    //         transition(':enter', [
    //             animate('200ms ease-in')
    //         ]),
    //         transition(':leave', [
    //             animate('20ms ease-out')
    //         ])
    //     ])
    // ]
})
export class FilepickerComponent implements OnInit, OnChanges {

    @Input() directory = '';
    @Input() showHidden = false;
    @Input() download = false;
    @Input() navigate = true;
    @Input() showBackArrow = true;

    /**
     * refreshInterval is specified in seconds
     */
    @Input() refreshInterval = 60;

    public listing: Observable < RicherListing | null > = of (null);
    public errorMessage: string | null = null;
    public showBack = false;

    @Output() path = new EventEmitter < string > ();

    constructor(
        private _auth: AuthService,
        private _oc: OwncloudService,
        private _formatter: HumanreadableService,
    ) {}

    ngOnInit() {
        this._listDirectory(this.directory);
        this._auth.addLogInEventListener(() => this._listDirectory(this.directory));
        this._auth.addLogOutEventListener(() => this._listDirectory(this.directory));
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['showHidden'] || changes['directory'] || changes['refreshInterval'] || changes['showBackArrow']) {
            this._listDirectory(this.directory);
        }
    }

    private _listDirectory(path: string) {
        // polling
        this.listing = timer(0, this.refreshInterval * 1000).pipe(
            tap(() => this.errorMessage = null),
            mergeMap(() => this._auth.user()),
            filter(user => {
                if (user === null) {
                    // simulates an error
                    this._catchError({
                        status: 401
                    } as HttpErrorResponse);
                    return false;
                }
                return true;
            }),
            mergeMap(() => this._oc.list(path, this.showHidden)),
            map(list => this._formatListing(list)),
            catchError(error => this._catchError(error)),
        );
    }

    handleFileDir(file: RicherFileListing) {
        const path = file.parentPath + file.name;

        if (this.download && file.type === 'file') {
            this._oc.downloadFile(path).pipe(
                first(),
            ).subscribe();
        }

        if (this.navigate && file.type !== 'file') {
            this._listDirectory(path);
        }

        this.path.emit(path);
    }

    goBack(path: RicherListing) {

        // Directories ends on '/' -> /1/2/3/ -> dirname('/1/2/3') -> '/1/2'
        const parentPath = dirname(path.directory.replace(/\/$/, ''));

        if (this.navigate) {
            this._listDirectory(parentPath);
        }

        this.path.emit(parentPath);
    }

    private _formatListing(list: DirectoryListing): RicherListing {
        const newList = list as RicherListing;

        this.showBack = false;
        if (list.directory !== '/') {
            this.showBack = true;
        }

        newList.files.forEach(file => {
            file.lastModified = this._formatter.timeDifferenceFromNow(file.mtime);
            file.humanSize = this._formatter.fileSize(file.size);
            file.icon = file.type === 'dir' ? 'folder' : 'insert_drive_file';
        });

        return newList;
    }

    private _catchError(error: HttpErrorResponse): Observable < null > {
        switch (error.status) {
            case 401:
                this.errorMessage = 'Please, login to see your files.';
                break;
            case 404:
                this.errorMessage = 'Sorry, file or document not found.';
                break;
            default:
                this.errorMessage = 'Sorry, there was an unknown error. Please, try again.';
        }

        return of(null);
    }
}
