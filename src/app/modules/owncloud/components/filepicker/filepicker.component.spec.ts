import {
    async,
    ComponentFixture,
    TestBed
} from '@angular/core/testing';

import {
    CommonStub
} from 'testing/common';

import {
    FilepickerComponent
} from './filepicker.component';

describe('FilepickerComponent', () => {
    let component: FilepickerComponent;
    let fixture: ComponentFixture < FilepickerComponent > ;

    beforeEach(async (() => {
        TestBed.configureTestingModule({
                imports: [
                    CommonStub
                ]
            })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FilepickerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
