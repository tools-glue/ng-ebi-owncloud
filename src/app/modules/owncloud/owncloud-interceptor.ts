import {
    HTTP_INTERCEPTORS,
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest
} from '@angular/common/http';
import {
    Injectable,
    Inject,
    InjectionToken
} from '@angular/core';
import {
    Observable
} from 'rxjs';
import {
    first,
    map,
    mergeMap,
} from 'rxjs/operators';

import {
    AuthService,
    User
} from 'ng-ebi-authorization';
import {
    OC_CONFIG,
    OwncloudConfig
} from './owncloud.config';

/**
 * This interceptors adds `Authorizaton Basic` header.
 * In ownCloud version 10 a proper plugin will
 * be created that will support `Authorization Bearer`. At that point, we will
 * use JwtModule interceptor and remove this dependency.
 */
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    private _user: Observable < User | null > ;
    private readonly _ocURL: string;

    constructor(
        @Inject(OC_CONFIG) private _config: OwncloudConfig,
        private _auth: AuthService
    ) {
        this._ocURL = _config.owncloudURL.replace(/\/$/, '');
        this._user = _auth.user();
    }

    intercept(req: HttpRequest < any > , next: HttpHandler):
        Observable < HttpEvent < any >> {
            if (!req.url.startsWith(this._ocURL)) {
                return next.handle(req);
            }


            return this._user.pipe(
                first(),
                mergeMap(user => {
                    if (user !== null) {
                        const encodedToken = btoa(user.uid + ':' + user.token);
                        const authToken = `Basic ${encodedToken}`;
                        req = req.clone({
                            setHeaders: {
                                Authorization: authToken
                            }
                        });
                    }

                    return next.handle(req);
                }),
            );
        }
}

/** Http interceptor providers in outside-in order */
export const OwncloudInterceptor = [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
}, ];
