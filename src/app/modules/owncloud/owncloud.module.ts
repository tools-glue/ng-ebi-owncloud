import {
    NgModule,
    ModuleWithProviders
} from '@angular/core';
import {
    CommonModule
} from '@angular/common';
import {
    MatIconModule,
    MatListModule,
} from '@angular/material';

import {
    OwncloudConfig,
    OC_CONFIG
} from './owncloud.config';
// JwtModule has a custom Http interceptor. However, it doesn't work for the
// EBI ownCloud as it uses `Authorization Basic`.
import {
    OwncloudInterceptor
} from './owncloud-interceptor';
import {
    FilepickerComponent
} from './components/filepicker/filepicker.component';
import {
    OwncloudService
} from './services/owncloud.service';
import {
    HumanreadableService
} from './services/humanreadable.service';

@NgModule({
    imports: [
        CommonModule,
        MatIconModule,
        MatListModule,
    ],
    declarations: [
        FilepickerComponent,
    ],
    exports: [
        FilepickerComponent,
    ],
    providers: [
        HumanreadableService
    ]
})
export class OwncloudModule {
    static forRoot(options: OwncloudConfig): ModuleWithProviders {
        return {
            ngModule: OwncloudModule,
            providers: [{
                    provide: OC_CONFIG,
                    useValue: options
                },
                OwncloudService,
                OwncloudInterceptor,
            ]
        };
    }

    // To provides the module with the component, but not essential services.
    static forChild(): ModuleWithProviders {
        return { ngModule: OwncloudModule };
    }
}
