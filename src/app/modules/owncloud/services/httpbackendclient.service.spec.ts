import {
    TestBed,
    inject
} from '@angular/core/testing';
import {
    HttpClient,
    HttpBackend
} from '@angular/common/http';

import {
    HttpBackendClient
} from './httpbackendclient.service';

describe('HttpBackendClient', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                HttpClient,
                HttpBackend,
                HttpBackendClient,
            ]
        });
    });

    it('should be created', inject([HttpBackendClient], (service: HttpBackendClient) => {
        expect(service).toBeTruthy();
    }));
});
