import {
    TestBed,
    inject
} from '@angular/core/testing';
import {
    HttpClientModule
} from '@angular/common/http';

import {
    CommonStub
} from 'testing/common';

import {
    OwncloudService
} from './owncloud.service';

describe('OwncloudService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonStub,
            ],
            providers: [OwncloudService]
        });
    });

    it('should be created', inject([OwncloudService], (service: OwncloudService) => {
        expect(service).toBeTruthy();
    }));
});
