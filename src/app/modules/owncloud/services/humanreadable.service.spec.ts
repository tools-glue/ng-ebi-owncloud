import {
    TestBed,
    inject
} from '@angular/core/testing';

import {
    HumanreadableService
} from './humanreadable.service';

describe('HumanreadableService', () => {
    let service: HumanreadableService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [HumanreadableService]
        });
    });

    beforeEach(inject([HumanreadableService], (serv: HumanreadableService) => {
        service = serv;
    }));

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should be produce correct time string differences ', () => {
        const now = new Date().valueOf();
        let difference = service.timeDifferenceFromNow(now);
        expect(difference).toEqual('less than a minute ago');

        const oneMonth = 30 * 24 * 60 * 60 * 1000;
        difference = service.timeDifferenceFromNow(now - oneMonth);
        expect(difference).toEqual('about 1 month ago');

        const oneYear = 365 * 24 * 60 * 60 * 1000;
        difference = service.timeDifferenceFromNow(now - oneYear);
        expect(difference).toEqual('about 1 year ago');
    });

    it('should be produce correct file sizes ', () => {
        expect(service.fileSize(1)).toEqual('1 B');
        expect(service.fileSize(999)).toEqual('999 B');

        expect(service.fileSize(1000)).toEqual('1.0 kB');
        expect(service.fileSize(1040)).toEqual('1.0 kB');
        expect(service.fileSize(1050)).toEqual('1.1 kB');
        expect(service.fileSize(1949)).toEqual('1.9 kB');
        expect(service.fileSize(2000)).toEqual('2.0 kB');

        expect(service.fileSize(1000000)).toEqual('1.0 MB');
        expect(service.fileSize(1040000)).toEqual('1.0 MB');
        expect(service.fileSize(1050000)).toEqual('1.1 MB');

        expect(service.fileSize(1, false)).toEqual('1 B');
        expect(service.fileSize(999, false)).toEqual('999 B');
        expect(service.fileSize(1023, false)).toEqual('1023 B');

        expect(service.fileSize(1024, false)).toEqual('1.0 KiB');
        expect(service.fileSize(1075, false)).toEqual('1.0 KiB');
        expect(service.fileSize(1076, false)).toEqual('1.1 KiB');
        expect(service.fileSize(1996, false)).toEqual('1.9 KiB');
        expect(service.fileSize(1997, false)).toEqual('2.0 KiB');
        expect(service.fileSize(2048, false)).toEqual('2.0 KiB');
    });
});
