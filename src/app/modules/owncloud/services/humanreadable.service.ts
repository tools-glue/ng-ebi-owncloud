import {
    Injectable
} from '@angular/core';

import {
    formatDistance
} from 'date-fns';

@Injectable()
export class HumanreadableService {

    constructor() {}

    public timeDifferenceFromNow(timestamp: number): string {
        return formatDistance(timestamp, new Date(), {addSuffix: true});
    }

    public fileSize(bytes: number, si: boolean = true): string {
        const thresh = si ? 1000 : 1024;

        if (Math.abs(bytes) < thresh) {
            return bytes + ' B';
        }

        const units = si ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
        let u = -1;
        do {
            bytes /= thresh;
            ++u;
        } while (Math.abs(bytes) >= thresh && u < units.length - 1);
        return bytes.toFixed(1) + ' ' + units[u];
    }
}
