import {
    Injectable,
    Inject,
    RendererFactory2,
    Renderer2
} from '@angular/core';
import {
    HttpClient,
    HttpHeaders,
    HttpResponse
} from '@angular/common/http';
import {
    Observable,
    pipe
} from 'rxjs';
import {
    concatMap,
    filter,
    map,
    tap,
    first
} from 'rxjs/operators';

import {
    OC_CONFIG,
    OwncloudConfig
} from '../owncloud.config';
import {
    AuthService,
    User
} from 'ng-ebi-authorization';

export class DirectoryListing {
    constructor(
        public directory: string,
        public isListingAFile: boolean,
        public files: FileListing[],
    ) {}
}

export class FileListing {
    constructor(
        public parentPath: string,
        public mtime: number,
        public name: string,
        public mimetype: string,
        public size: number,
        public type: 'dir' | 'file',
        public etag: string
    ) {}
}

export interface PatchProp {
    [key: string]: string;
}

/**
 * Removes whitespaces and slashes from both ends of the path
 *
 * @param path Path
 *
 * @return Sanitased path
 */
export function trimEncode(path: string) {
    const trimmedPath = path.trim()
        .replace(/^\//, '')
        .replace(/\/$/, '');
    const encodedPath = trimmedPath.split('/')
        .map(level => encodeURIComponent(level));
    return encodedPath.join('/');
}

/**
 * Decorator that sanitise the path parameter (first argument of the
 * method) and transforms it into a URL.
 */
export function transformPath(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const oldMethod = descriptor.value;

    descriptor.value = function(...args: any[]) {
        const instance = < OwncloudService > this;
        if (args.length) {
            args[0] = `${instance._ocAPI}/${trimEncode(args[0])}`;
        } else {
            // Default path is the ownCloud URL API
            args[0] = instance._ocAPI;
        }
        return oldMethod.apply(instance, args);
    };

    return descriptor;
}

/**
 * Returns the basename of a path.
 * /.a/.b -> .b
 * ../.a/.b -> .b
 * .b -> .b
 * /.a/.b/ -> ''
 *
 * @param path String representing the path
 * @return Empty string if no basename
 */
export function basename(path: string): string {
    return path.replace(/.*\//, '');
}

/**
 * Returns the dirname of a path.
 * /.a/.b -> /.a/
 * ../.a/.b -> ../.a/
 * ../.a/.b/ -> ../.a/.b/
 * / -> /
 * ../ -> ../
 * .b -> ''
 *
 * @param path String representing the path
 * @return Empty string if no basename
 */
export function dirname(path: string): string {
    const match = path.match(/.*\//);
    return match ? match[0] : '';
}

@Injectable()
export class OwncloudService {
    private readonly renderer: Renderer2;
    private readonly _ocURL: URL;
    private readonly _webDAVAPI = 'remote.php/webdav';
    private readonly _user: Observable < User | null > ;
    public readonly _ocAPI: string;

    /**
     * Checks Http status code for successful file creation or overwriting.
     *
     * @param status Http code
     *
     * @return True if file or directory has been created or overwritten, otherwise false
     */
    private static _checkCreatedOrOverwritten(status: number): boolean {
        return (status === 201 || status === 204) ? true : false;
    }

    /**
     * Formats the body for the PROPPATCH request.
     *
     * @param properties Each property should be namespaced.
     *        There are two namespaces xmlns:d='DAV:' and xmlns:oc='http://owncloud.org/ns'.
     *        Example:
     *          {
     *             'd:lastmodified': 'Fri, 13 Feb 2015 00:00:00 GMT',
     *             'oc:favorite': '1',
     *          }
     *
     * @return Request body
     */
    private static _constructPatchBody(properties: PatchProp): string {
        const formattedProps = this._formatProperties(properties);

        return `<?xml version='1.0'?>
    <d:propertyupdate xmlns:d='DAV:' xmlns:oc='http://owncloud.org/ns'>
      <d:set>
        <d:prop>
${formattedProps}
        </d:prop>
      </d:set>
    </d:propertyupdate>
    `;
    }

    /**
     * Formats the body for the PROPFIND request.
     *
     * @param properties Each property should be namespaced.
     *        There are two namespaces xmlns:d='DAV:' and xmlns:oc='http://owncloud.org/ns'.
     *        Example: 'd:getlastmodified', 'd:getcontenttype', 'oc:favorite'
     *
     * @return Request body
     */
    private static _constructFindBody(properties: string[]): string {
        const formattedProps = properties.map(p => `<${p} />`);

        return `<?xml version='1.0'?>
    <d:propfind xmlns:d='DAV:' xmlns:oc='http://owncloud.org/ns'>
      <d:prop>
${formattedProps.join('\n')}
      </d:prop>
    </d:propfind>
    `;
    }

    /**
     * Formats the body for the REPORT request.
     *
     * @param properties Each property should be namespaced.
     *        There are two namespaces xmlns:d='DAV:' and xmlns:oc='http://owncloud.org/ns'.
     *        Example:
     *          {
     *             'd:lastmodified': 'Fri, 13 Feb 2015 00:00:00 GMT',
     *             'oc:favorite': '1',
     *          }
     *
     * @return Request body
     */
    private static _constructReportBody(properties: PatchProp): string {
        const formattedProps = this._formatProperties(properties);

        return `<?xml version='1.0'?>
    <oc:filter-files xmlns:d='DAV:' xmlns:oc='http://owncloud.org/ns'>
      <oc:filter-rules>
${formattedProps}
      </oc:filter-rules>
    </oc:filter-files>
    `;
    }

    /**
     * Transform property object into string.
     *
     * @param properties Property object
     *
     * @return properly formatted string
     */
    private static _formatProperties(properties: PatchProp): string {
        const keys = Object.keys(properties); // To avoid problem packaging the library
        return keys.map(k => `<${k}>${properties[k]}</${k}>`).join('\n');
    }

    /**
     * Parses XML response from a PROPFIND request and transforms it into a
     * DirectoryListing structure
     *
     * @param dom Response from PROPFIND to be parsed
     *
     * @param showHidden Either to show hidden files or directories or not.
     *
     * @return Listing of directory
     */
    private static _createDirectoryListing(dom: XMLDocument, showHidden: boolean, webDAVAPI: string):
        DirectoryListing {
            const files: FileListing[] = [];
            const dir = new DirectoryListing('', false, files);

            if (dom.documentElement === null) {
                return dir;
            }

            // Document structure:
            // root
            //   d: response -- First level --
            //     d:href
            //     d:propstat
            //   d: response -- Other level --
            //     d:href
            //     d:propstat
            // First level is warranty.
            // Other levels only if the first level is a directory and has content

            Array.from(dom.documentElement.children).map((element, index) => {
                const fullPath = element.getElementsByTagName('d:href')[0]
                    .innerHTML
                    .replace(`/${webDAVAPI}`, '');

                const dirName = dirname(fullPath);
                const fileName = basename(fullPath);
                let name = decodeURIComponent(fileName);

                const lastModified = element.getElementsByTagName('d:getlastmodified')[0].innerHTML;
                const lastModifiedDate = new Date(lastModified);
                const epoch = lastModifiedDate.getTime();
                const etag = element.getElementsByTagName('d:getetag')[0].innerHTML;

                let size: number;
                let mimetype = '';
                let filetype: 'dir' | 'file';

                if (element.getElementsByTagName('d:collection').length) {
                    name = decodeURIComponent(basename(dirName.slice(0, -1)));
                    filetype = 'dir';
                    size = Number(element.getElementsByTagName('d:quota-used-bytes')[0].innerHTML);
                } else {
                    filetype = 'file';
                    size = Number(element.getElementsByTagName('d:getcontentlength')[0].innerHTML);
                    mimetype = element.getElementsByTagName('d:getcontenttype')[0].innerHTML;
                }

                if (showHidden === false && name.startsWith('.')) {
                    return;
                }

                const file = new FileListing(
                    dir.directory,
                    epoch,
                    name,
                    mimetype,
                    size,
                    filetype,
                    etag
                );

                // First level parsing
                if (index === 0) {
                    dir.directory = dirName;
                    if (filetype === 'file') {
                        dir.isListingAFile = true;
                        files.push(file);
                    }
                } else {
                    files.push(file);
                }

            });

            return dir;
        }

    constructor(
        @Inject(OC_CONFIG) private _config: OwncloudConfig,
        private _http: HttpClient,
        private _auth: AuthService,
        private _rendererFactory: RendererFactory2,
    ) {
        this._ocURL = new URL(_config.owncloudURL);

        if (this._ocURL.protocol !== 'https:') {
            throw new Error('owncloudURL must use secure protocol (https)');
        }

        this._ocAPI = `${this._ocURL}${this._webDAVAPI}`;
        this._user = _auth.user();

        this.renderer = this._rendererFactory.createRenderer(null, null);
    }

    /**
     * List a file or the content of a directory.
     *
     * @param path Path of the file or directory, v. gr.: '/Documents/Subfolder'. Use '/' or empty string for root.
     * @param showHidden By default it does not list hidden files and directories
     *
     * @return Observable with the listing
     *
     * @throws HttpErrorResponse if problems with network, user is not logged in, or file/directory doesn't exist.
     */
    public list(path = '/', showHidden = false): Observable < DirectoryListing > {
        return this.propFind(path).pipe(
            map(response => {
                const parser = new DOMParser();
                return parser.parseFromString(response.body as string, 'text/xml');
            }),
            map(dom => OwncloudService._createDirectoryListing(dom, showHidden, this._webDAVAPI))
        );
    }

    /**
     * Saves or overwrites a file.
     *
     * @param path Path of the file or directory, v. gr.: '/Documents/Subfolder/file.json'
     * @param content Content of the file to be created or overwritten.
     *
     * @return True if able to be created or overwrite it, otherwise false
     *
     * @throws HttpErrorResponse if problems with network, user is not logged in, or file couldn't be created.
     */
    @transformPath
    saveFile(path: string, content: any): Observable < boolean > {
        return this._http.put(path, content, {
            observe: 'response'
        }).pipe(
            map(response => OwncloudService._checkCreatedOrOverwritten(response.status))
        );
    }

    /**
     * Read the content of a file.
     *
     * @param path Path of the file or directory, v. gr.: '/Documents/Subfolder/file.json'
     *
     * @returns Content of the file in text format.
     *
     * @throws HttpErrorResponse if problems with network, user is not logged in, or file couldn't be read.
     */
    @transformPath
    readFile(path: string, text = true): Observable < string > {
        return this._http.get(path, {
            responseType: 'text'
        });
    }

    /**
     * Delete a file or a directory (recursively).
     *
     * @param path Path of the file or directory, v. gr.: '/Documents/Subfolder'
     *
     * @return True if able to be deleted, otherwise false
     *
     * @throws HttpErrorResponse if problems with network, user is not logged in, or file couldn't be deleted.
     */
    @transformPath
    delete(path: string): Observable < boolean > {
        return this._http.delete(path, {
            observe: 'response'
        }).pipe(
            map(response => response.status === 204 ? true : false)
        );
    }

    /**
     * Create directory (non-recursive, unlike `mkdir -p`).
     *
     * @param path Path of the file or directory, v. gr.: '/Documents/Subfolder'
     *
     * @return True if able to be create the directory, otherwise false
     *
     * @throws HttpErrorResponse if problems with network, user is not logged in, or file couldn't be deleted.
     *         It throws a 405 (Method Not Allowed) when trying to create a directory that already exists.
     */
    @transformPath
    createDirectory(path: string): Observable < boolean > {
        return this._http.request('MKCOL', path, {
            observe: 'response'
        }).pipe(
            map(response => response.status === 201 ? true : false)
        );
    }

    /**
     * Copy directory or file to new destination. When a directory the content is copied recursively.
     *
     * @param path Path of the file or directory, v. gr.: '/Documents/Subfolder/file.json'
     * @param destination Path should contain the name of the final
     *        file or directory, v. gr.: '/Documents/NEW_NAME.txt'
     *
     * @return True if able to copy the directory or file, otherwise false
     *
     * @throws HttpErrorResponse if problems with network, user is not logged in, or file couldn't be deleted.
     */
    @transformPath
    copy(path: string, destination: string): Observable < boolean > {
        const cleanDestination = `${this._ocAPI}/${trimEncode(destination)}`;
        return this._http.request('COPY', path, {
            headers: new HttpHeaders({
                'Destination': cleanDestination
            }),
            observe: 'response'
        }).pipe(
            map(response => OwncloudService._checkCreatedOrOverwritten(response.status))
        );
    }

    /**
     * Move directory or file to new destination.
     * The new destination preserves the properties of the original file/directory.
     *
     * @param path Path of the file or directory, v. gr.: '/Documents/Subfolder/file.json'
     * @param destination Path should contain the name of the final
     *        file or directory, v. gr.: '/Documents/NEW_NAME.txt'
     *
     * @return True if able to move the directory or file, otherwise false
     *
     * @throws HttpErrorResponse if problems with network, user is not logged in, or file couldn't be deleted.
     */
    @transformPath
    move(path: string, destination: string): Observable < boolean > {
        const cleanDestination = `${this._ocAPI}/${trimEncode(destination)}`;
        return this._http.request('MOVE', path, {
            headers: new HttpHeaders({
                'Destination': cleanDestination
            }),
            observe: 'response'
        }).pipe(
            map(response => OwncloudService._checkCreatedOrOverwritten(response.status))
        );
    }

    /**
     * Update the properties of a file or directory.
     *
     * @param path Path of the file or directory, v. gr.: '/Documents/Subfolder/file.json'
     * @param properties New properties to be applied, v. gr.: {'d:lastmodified': 'Fri, 13 Feb 2015 00:00:00 GMT'}.
     * 'd:' refers to DAV and 'oc:' to specific ownCloud properties.
     *
     * @return True if able to update the directory or file property metadata, otherwise false
     *
     * @throws HttpErrorResponse if problems with network, user is not logged in, or file couldn't be deleted.
     */
    @transformPath
    propPatch(path: string, properties: PatchProp): Observable < boolean > {
        const body = OwncloudService._constructPatchBody(properties);

        return this._http.request('PROPPATCH', path, {
            body: body,
            responseType: 'text',
            observe: 'response'
        }).pipe(
            map(response => response.status === 207 ? true : false)
        );
    }

    /**
     * Find the properties of a file or directory.
     *
     * @param path Path of the file or directory, v. gr.: '/Documents/Subfolder/file.json'
     * @param properties List of properties to searched, v. gr.: 'd:lastmodified', 'oc:favorite'
     *
     * @return Http response
     *
     * @throws HttpErrorResponse if problems with network, user is not logged in, or file couldn't be deleted.
     */
    @transformPath
    propFind(path: string, ...properties: string[]): Observable < HttpResponse < string >> {
        const body = OwncloudService._constructFindBody(properties);

        return this._http.request('PROPFIND', path, {
            body: body,
            responseType: 'text',
            observe: 'response'
        });
    }

    /**
     * UNTESTED: List files in a directory with the specified property.
     *
     * @param path Directory to report, v. gr.: '/Documents/Subfolder'
     * @param properties Filter files with these properties,  v. gr.: {'oc:favorite': '1'}.
     *
     * @return Http response
     *
     * @throws HttpErrorResponse if problems with network, user is not logged in, or file couldn't be deleted.
     */
    @transformPath
    report(path: string, properties: PatchProp): Observable < HttpResponse < string >> {
        const body = OwncloudService._constructReportBody(properties);

        return this._http.request('REPORT', path, {
            body: body,
            responseType: 'text',
            observe: 'response'
        });
    }

    /**
     * Force to download a file in the client.
     *
     * @param path Path of the file, v. gr.: '/Documents/Subfolder/file.json'
     */
    @transformPath
    public getLink(path: string): Observable < string > {
        return this._user.pipe(
            first(),
            // https://github.com/ReactiveX/rxjs/issues/2927
            // https://github.com/Microsoft/TypeScript/issues/18562
            filter((user: any): user is User => user !== null),
            map(user => {
                const inlineUser = `${this._ocURL.protocol}//${user.uid}:${user.token}@`;
                return path.replace(`${this._ocURL.protocol}//`, inlineUser);
            }),
        );
    }

    /**
     * Reports if the path represents a directory.
     *
     * @param path Path of the file or directory, v. gr.: '/Documents/Subfolder/file.json'
     */
    public isDirectory(path: string): Observable < boolean > {
        return this.list(path).pipe(
            first(),
            map(list => list.isListingAFile ? false : true),
        );
    }

    /**
     * Force to download a file in the client.
     *
     * @param path Path of the file, v. gr.: '/Documents/Subfolder/file.json'. Directories are ignored
     *
     * @throws HttpErrorResponse if problems with network, user is not logged in, or file couldn't be read.
     */
    public downloadFile(path: string): Observable < string > {
        return this.isDirectory(path).pipe(
            // Discard path that represent directories
            filter(isDirectory => isDirectory === false),
            concatMap(list => this.getLink(path)),
            tap(link => this._createAnchor(link, path)),
        );
    }

    /**
     * Create and delete invisible anchor elements that can be used to trigger donwload
     *
     * @param link URL with the credentials.
     * @param path File path.
     */
    private _createAnchor(link: string, path: string): void {
        const anchor = this.renderer.createElement('a');
        this.renderer.setProperty(anchor, 'href', link);
        // The following is safer because if something goes wrong it
        // displays message in another window. However, it is not pretty.
        // It also only works if pop ups are active for the domain
        // this.renderer.setProperty(anchor, 'target', '_blank');
        anchor.click();
    }

}
