import {
    Component,
    OnInit
} from '@angular/core';
import {
    Router,
    ActivatedRoute,
} from '@angular/router';
import {
    concatMap,
    delay,
    map,
    tap,
} from 'rxjs/operators';
import {
    pipe,
    of,
    range
} from 'rxjs';
import {
    HttpClient
} from '@angular/common/http';

import {
    AuthService,
} from 'ng-ebi-authorization';
import {
    OwncloudService,
    DirectoryListing,
    dirname,
} from './modules/owncloud/services/owncloud.service';
import {
    HttpBackendClient
} from './modules/owncloud/services/httpbackendclient.service';


@Component({
    selector: 'oc-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    public directory = 'Documents';
    public showHidden = true;
    public navigate = false;
    public showBackArrow = true;

    constructor(
        // Public for demonstration purposes
        public auth: AuthService,
        private oc: OwncloudService,

        // To test interceptors
        private _http: HttpClient,
        // private _http: HttpBackendClient,

        // To test component with router
        private _route: ActivatedRoute,
        private _router: Router,
    ) { }

    ngOnInit() {
        // To test that the specialised Http client is free of interceptors.
        // this._http.get('http://localhost:4200', {responseType: 'text'}).subscribe(x => console.log(x));
        // this._http.get('https://oc.ebi.ac.uk/remote.php/webdav/ownCloud%20Manual.pdf', {responseType: 'text'}).subscribe();
        // this._http.get('https://dev.flow.toolchain.ebi.ac.uk/jobs/186', {responseType: 'text'}).subscribe(data => console.log(data));

        // this._route.url.pipe(
        //     tap(path =>{ debugger })
        // ).subscribe();

        // window.setTimeout(() => {
        //     console.log('Changed "directory" property');
        //     this.directory = 'Photos';
        // }, 2000);

        // window.setTimeout(() => {
        //     console.log('Changed "navigate" property');
        //     this.navigate = true;
        // }, 5000);

        // window.setTimeout(() => {
        //     console.log('Changed "showHidden" property');
        //     this.showHidden = false;
        // }, 10000);
    }

    testOwnCloud() {
        // Test ownCloud service
        const allTest = pipe(
                this._testComprehensive(),
                this._testCopyFolders(),
                this._testMoveFolders(),
                this._testRecursiveCopyFolders(),
                this._testDownload(),
        );

        range(1, 2).pipe(
            concatMap((value, index) => of(value).pipe(
                tap(round => console.log(`Start round #${round}`)),
                allTest,
                tap(() => console.log('Finished')),
            )),
        ).subscribe();
    }

    public onPath(path: string): void {
        console.log('New path selected: ', path);
    }

    /**
     * Test ownCloud by executing a series of operations and asserting results.
     */
    private _testComprehensive() {

        const newDirectory = '/test 1';
        const newFile = `${newDirectory}/My New File.txt`;
        const fileCopy = `${newDirectory}/My New File copy.txt`;
        const fileTmp = 'dummy.txt';
        const fileContent = 'Test';

        return pipe(
            this._testDirectoryCreation(newDirectory),
            this._testFileCreation(newFile, fileContent),
            this._testReadFile(newFile, fileContent),
            this._testProperties(newFile, 'd:getcontenttype'),
            this._testCopy(newFile, fileCopy),
            this._testMove(fileCopy, fileTmp),
            this._testMove(fileTmp, fileCopy),
            this._testDelete(newFile),
            this._testDelete(newDirectory),
        );
    }

    private _testCopyFolders() {

        const newDirectory = '/test 1';
        const newDirectory2 = '/test 2';
        const newFile = `${newDirectory}/My New File.txt`;
        const newFile2 = `${newDirectory2}/My New File.txt`;
        const fileContent = 'Test copy';

        return pipe(
            this._testDirectoryCreation(newDirectory),
            this._testFileCreation(newFile, fileContent),
            this._testCopy(newDirectory, newDirectory2),
            this._testReadFile(newFile2, fileContent),
            this._testDelete(newDirectory),
            this._testDelete(newDirectory2),
        );
    }

    private _testMoveFolders() {

        const newDirectory = '/test m';
        const newDirectory2 = '/test m2';
        const newFile = `${newDirectory}/My New File.txt`;
        const newFile2 = `${newDirectory2}/My New File.txt`;
        const fileContent = 'Test moving';

        return pipe(
            this._testDirectoryCreation(newDirectory),
            this._testFileCreation(newFile, fileContent),
            this._testMove(newDirectory, newDirectory2),
            this._testReadFile(newFile2, fileContent),
            this._testDelete(newDirectory2),
        );
    }

    private _testRecursiveCopyFolders() {

        const newDirectory = '/.test 1';
        const newDirectory2 = `${newDirectory}/test 2`;
        const newDirectory3 = `${newDirectory2}/test 3`;
        const newDirectory4 = `${newDirectory}/copy`;

        const newFile = `${newDirectory3}/My New File.txt`;
        const newFile2 = `${newDirectory4}/test 3/My New File.txt`;
        const fileContent = '{"this is json": "ignored"}';

        return pipe(
            this._testDirectoryCreation(newDirectory),
            this._testDirectoryCreation(newDirectory2),
            this._testDirectoryCreation(newDirectory3),
            this._testFileCreation(newFile, fileContent),
            this._testCopy(newDirectory2, newDirectory4),
            this._testReadFile(newFile2, fileContent),
            this._testDelete(newDirectory),
        );
    }

    private _testDownload() {
        const newFile = 'download.txt';
        const fileContent = 'to be downloaded';
        return pipe(
            this._testFileCreation(newFile, fileContent),
            concatMap(() => this.oc.downloadFile(newFile)),
            this._testDelete(newFile),
        );
    }

    private _testDirectoryCreation(directory: string) {
        const createDir = pipe(
            concatMap(() => this.oc.createDirectory(directory)),
            tap(hasSucceded => console.assert(hasSucceded, `unable to create directory "${directory}".`)),

            this._numberFiles(directory),
            // string substitution doesn't work in assert with Chrome
            tap(numberFiles => console.assert(numberFiles === 0,
                `"${directory}" should contain 0 files but it contains ${numberFiles} files.`)),
        );

        return this._testOneMoreFile(dirname(directory),
            this._testTimeCreation(directory, createDir));
    }

    private _testFileCreation(newFile: string, fileContent: string) {
        const createFile = pipe(
            concatMap(() => this.oc.saveFile(newFile, fileContent)),
            tap(hasSucceded => console.assert(hasSucceded, `unable to create file "${newFile}".`)),

            this._testProperties(newFile, 'd:getcontenttype'),
            tap(dom => {
                const contenttype = dom.getElementsByTagName('d:getcontenttype')[0].innerHTML;
                console.assert(contenttype === 'text/plain',
                    `"${newFile}" expected content type to be "text/plain" but got "${contenttype}".`);
            }),
        );

        return this._testOneMoreFile(dirname(newFile),
            this._testTimeCreation(newFile, createFile));
    }

    private _testReadFile(file: string, fileContent: string) {
        return pipe(
            concatMap(() => this.oc.readFile(file)),
            tap(content => console.assert(content === fileContent,
                `"${file}" expected content to be "${fileContent}" but got "${content}".`)),
        );
    }

    private _testProperties(path: string, ...props: string[]) {
        return pipe(
            concatMap(() => this.oc.propFind(path, ...props)),
            map(response => {
                const parser = new DOMParser();
                return parser.parseFromString(response.body as string, 'text/xml');
            }),
        );
    }

    private _testPropertyModifications(path: string) {
        const newDateValue = 'Fri, 13 Feb 2015 00:00:00 GMT';
        const newFavoriteValue = '1';

        return pipe(
            concatMap(() => this.oc.propPatch(path, {
                'd:lastmodified': newDateValue,
                'oc:favorite': newFavoriteValue,
            })),
            tap(hasSucceded => console.assert(hasSucceded,
                `unable to set properties "d:lastmodified" and "oc:favorite" for "${path}".`)),

            this._testProperties(path, 'd:getlastmodified', 'oc:favorite'),
            tap(dom => {
                const lastModified = dom.getElementsByTagName('d:getlastmodified')[0].innerHTML;
                console.assert(lastModified === newDateValue,
                    `"${path}" expected last modification date to be "${newDateValue}" but got "${lastModified}".`);
            }),
            tap(dom => {
                const favorite = dom.getElementsByTagName('oc:favorite')[0].innerHTML;
                console.assert(favorite === newFavoriteValue,
                    `"${path}" expected oc:favorite property to be "${newFavoriteValue}" but got "${favorite}".`);
            }),
        );
    }

    private _testCopy(origin: string, destination: string) {
        const copyFile = pipe(
            concatMap(() => this.oc.copy(origin, destination)),
            tap(hasSucceded => console.assert(hasSucceded,
                `unable to copy "${origin}" to "${destination}".`)),
        );

        // TODO: it will fail test if destination exists
        return this._testOneMoreFile(dirname(destination),
            this._testTimeCreation(destination, copyFile));
    }

    private _testMove(origin: string, destination: string) {
        const copyFile = pipe(
            concatMap(() => this.oc.move(origin, destination)),
            tap(hasSucceded => console.assert(hasSucceded, `unable to move "${origin}" to "${destination}".`)),
        );
        const dirOrigin = dirname(origin);
        const dirDestination = dirname(destination);

        if (dirOrigin === dirDestination) {
            return this._testEqualNumberFiles(dirDestination,
                this._testTimeCreation(destination, copyFile, false));
        }

        // TODO: it will fail test if destination exists
        return this._testOneLessFile(dirOrigin,
            this._testOneMoreFile(dirDestination,
                this._testTimeCreation(destination, copyFile, false)));
    }

    private _testDelete = (path: string) => {
        const deleteDir = pipe(
            concatMap(() => this.oc.delete(path)),
            tap(hasSucceded => console.assert(hasSucceded, `unable to delete "${path}".`)),
        );

        return this._testOneLessFile(dirname(path), deleteDir);
    }

    private _numberFiles(directory: string) {
        return pipe(
            concatMap(content => this.oc.list(directory, true)),
            map(listing => listing.files.length),
        );
    }

    private _testOneMoreFile = (directory: string, operation: any) => {
        let expectedNumber = 0;

        return pipe(
            this._numberFiles(directory),
            tap(numberFiles => expectedNumber = numberFiles + 1),
            operation,
            this._numberFiles(directory),
            tap(numberFiles => console.assert(numberFiles === expectedNumber,
                `"${directory}" expected to contain ${expectedNumber} files but got ${numberFiles} files.`)),
        );
    }

    private _testOneLessFile(directory: string, operation: any) {
        let expectedNumber = 0;

        return pipe(
            this._numberFiles(directory),
            tap(numberFiles => expectedNumber = numberFiles - 1),
            operation,
            this._numberFiles(directory),
            tap(numberFiles => console.assert(numberFiles === expectedNumber,
                `"${directory}" expected to contain ${expectedNumber} files but got ${numberFiles} files.`)),
        );
    }

    private _testEqualNumberFiles(directory: string, operation: any) {
        let expectedNumber = 0;

        return pipe(
            this._numberFiles(directory),
            tap(numberFiles => expectedNumber = numberFiles),
            operation,
            this._numberFiles(directory),
            tap(numberFiles => console.assert(numberFiles === expectedNumber,
                `"${directory}" expected to contain ${expectedNumber} files but got ${numberFiles} files.`)),
        );
    }

    private _testTimeCreation(path: string, operation: any, testCreationTime = true) {
        let timeBeforeFileCreated: Date;
        let timeAfterFileCreated: Date;
        const millisecondsDelay = 1000;

        if (testCreationTime) {
            return pipe(
                tap(() => timeBeforeFileCreated = new Date()),
                delay(millisecondsDelay),
                operation,
                delay(millisecondsDelay),
                tap(() => timeAfterFileCreated = new Date()),
                this._testProperties(path, 'd:getlastmodified'),
                tap(dom => {
                    const lastModified = new Date(dom.getElementsByTagName('d:getlastmodified')[0].innerHTML);
                    console.assert(lastModified > timeBeforeFileCreated,
                        `"${path}" expected modification date "${lastModified}" to be newer than "${timeBeforeFileCreated}".`);
                    console.assert(timeAfterFileCreated > lastModified,
                        `"${path}" expected modification date "${lastModified}" to be older than "${timeAfterFileCreated}".`);
                }),
            );
        }

        return pipe(
            operation,
            delay(millisecondsDelay),
            tap(() => timeAfterFileCreated = new Date()),
            this._testProperties(path, 'd:getlastmodified'),
            tap(dom => {
                const lastModified = new Date(dom.getElementsByTagName('d:getlastmodified')[0].innerHTML);
                console.assert(timeAfterFileCreated > lastModified,
                    `"${path}" expected modification date "${lastModified}" to be older than "${timeAfterFileCreated}".`);
            }),
        );
    }
}
