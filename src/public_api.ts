export {
    DirectoryListing,
    FileListing,
    basename,
    dirname,
    OwncloudService
}
from './app/modules/owncloud/services/owncloud.service';
export {
    OwncloudModule
}
from './app/modules/owncloud/owncloud.module';
